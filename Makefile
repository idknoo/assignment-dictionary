flags = felf64

program: main.o dict.o lib.o
  ld -o program main.o dict.o lib.o

lib.o: lib.asm 
  nasm -f elf64 -o lib.o lib.asm

dict.o: dict.asm lib.inc
  nasm -f elf64 -o dict.o dict.asm

main.o: main.asm colon.inc words.inc
  nasm -f elf64 -o main.o main.asm

clean:
  rm *.o all
