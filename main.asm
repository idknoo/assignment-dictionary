;определите функцию _start, которая:
;Читает строку размером не более 255 символов в буфер с stdin.
;Пытается найти вхождение в словаре; если оно найдено, распечатывает в stdout значение по этому ключу.
;Иначе выдает сообщение об ошибке.
;Не забудьте, что сообщения об ошибках нужно выводить в stderr.
%include lib.inc

global _start	

section .data

;%define BUF_SIZE 256
BUFFER: times 256 db 0

section .rodata
	not_found_element: db "Элемент не найден", 0
	wrong_input: db "Не получилось загрузить слово", 0
    


section .text

%include "colon.inc"					
%include "words.inc"					

_start:							    
    mov rdi, BUFFER					
    mov rsi, 255						
    call read_word						
    test rax, rax; проверка rax == 0?
    je .wrong_input						
    mov rdi, rax						
    mov rsi, first_element; в rsi адрес начала словаря
    call find_word						
    test rax, rax						
    je .not_found_element				
    add rax, 8							
    mov rdi, rax						
    push rax							
    call string_length					
    pop rdi							   
    add rdi, rax; адрес начала ключа + длина самого ключа					
    inc rdi							    
    call print_string					
    jmp .end							

.wrong_input:							
    mov rdi, wrong_input					
    call print_error	
    jmp .end							

.not_found_element:						
    mov rdi, not_found_element			
    call print_error				

.end:							       
    call print_newline
    xor rax, rax					
    call exit		


