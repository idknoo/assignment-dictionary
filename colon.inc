%define first_element 0

%macro colon 2
	%ifstr %1
		%%new_element: 
			dq first_element
			db %1, 0
		%2:
			%define first_element %%new_element
	%else
		%error "First argument needs to be a string"
	%endif
%endmacro
