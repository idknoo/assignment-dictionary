section .text
 
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    string_length:
     xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length; получаем длину строки
    mov rdx, rax; передаем длину строки
    mov rsi, rdi; передаем строку
    mov rax, 1; передаем номер системного вызова
    mov rdi, 1; stdout
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi; сохранение в стеке символа
    mov rax, 1; передаем номер системного вызова
    mov rdi, 1; stdout
    mov rsi, rsp; передаем строку со стека
    mov rdx, 1; один символ
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA; записываем необходимый символ 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r9, 10
    mov rax, rdi
    mov r10, rsp
    dec rsp
.loop:
    xor rdx, rdx
    div r9
    mov rdi,rdx
    add rdi, '0'
    dec rsp
    mov byte [rsp], dil
    cmp rax, 0
    je .print
    jmp .loop
.print:
    mov rdi, rsp
    call print_string
    mov rsp, r10
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0
    jge .unsigned
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.unsigned:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
.loop:
    mov cl, byte[rdi+rax]
	cmp byte[rsi+rax], cl; byte comparison
	jne .not_equels ; if not equal, output 0
	cmp byte[rdi+rax], 0; end check 
	je  .equels; if the end, output 1
	inc rax 
	jmp .loop
.not_equels:
	mov rax, 0
	ret	 
.equels:
	mov rax, 1
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rcx, rcx 
.loop:
    push rcx
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    pop rcx
    cmp rax, 0x20
    je .first_read
    cmp rax, 0x9
    je .first_read
    cmp rax, 0xA
    je .first_read
    cmp rax, 0
    je .end
    mov [rdi + rcx], rax
    inc rcx
    cmp rcx, rsi
    jge .fail
    jmp .loop
.first_read:
	cmp rcx, 0
	je .loop
	jmp .end
.fail:
	xor rax, rax
	xor rdx, rdx
.end:
	xor rax, rax
	mov [rdi + rcx], rax
	mov rax, rdi
	mov rdx, rcx
	ret  

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
.loop:
    cmp byte[rdi], '9'; number should be less than 9
    ja .end
    cmp byte[rdi], '0' ; number should be greater than 9
    jb .end
    inc r8 ;
    imul rax, 10 ; *10 to add the number
    add al, byte[rdi] ; add a number
    sub al, '0'; 
    inc rdi ;  inc. the length counter
    jmp .loop
.end:
    mov rdx, r8 ; save the result
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-' ; проверка знака
    jne .unsigned
    inc rdi ; переход к следующей цифре
    call parse_uint
    neg rax ; изменение знака
    inc rdx ; инкремент (логично) счётчика
    ret
.unsigned:
    call parse_uint
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    xor r9, r9
.loop:
    cmp rax, rdx ; сравнение длины строки с длиной буфера
    je .error
    mov cl, byte[rdi + rax] ; 
    mov byte[rsi + rax], cl ; копируем символ в буфер
    cmp byte[rdi + rax], 0 ; проверяем конец
    je .end
    inc rax ; инкриментируем (логично)
    jmp .loop
.error:
    mov rax, 0
    ret
.end:
    ret

print_error:
    call string_length 
    mov rsi, rdi 
    mov rdx, rax 
    mov rax, 1 
    mov rdi, 2  
    syscall
    ret
