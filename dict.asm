; создать функцию find_word. Она принимает два аргумента:
;Указатель на нуль-терминированную строку.
;Указатель на начало словаря.
;find_word пройдёт по всему словарю в поисках подходящего ключа. 
;Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.
%include "colon.inc"
%include "lib.inc"

section .text

extern string_equals
global find_word

find_word:
    xor rax, rax
.loop:
    cmp rsi, 0
    je .not_found
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    je .end
    mov rsi, [rsi]
    jmp .loop
.not_found:
    xor rax, rax
    ret
.end:
    mov rax, rsi
    ret
