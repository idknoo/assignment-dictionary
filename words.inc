;хранить слова, определённые с помощью макроса colon. 
;Включите этот файл в main.asm

section .rodata

colon "third_word", third_word
db "Tredje (svenska)", 0

colon "second_word", second_word
db "Andra (svenska)", 0

colon "first_word", first_word
db "Första (svenska)", 0
